import { MYK } from "./constants.js";

export class Character extends Actor {
	/** @override */
	prepareData() {
		// Prepare data for the actor. Calling the super version of this executes
		// the following, in order: data reset (to clear active effects),
		// prepareBaseData(), prepareEmbeddedDocuments() (including active effects),
		// prepareDerivedData().
		super.prepareData();
	}

	/** @override */
	prepareBaseData() {
		// Data modifications in this step occur before processing embedded
		// documents or derived data.
	}

	/**
	 * @override
	 * Augment the basic actor data with additional dynamic data. Typically,
	 * you'll want to handle most of your calculated/derived data in this step.
	 * Data calculated in this step should generally not exist in template.json
	 * (such as ability modifiers rather than ability scores) and should be
	 * available both inside and outside of character sheets (such as if an actor
	 * is queried and has a roll executed directly from it).
	 */
	prepareDerivedData() {
		const actorData = this.data;
		const data = actorData.data;
		const flags = actorData.flags.myk || {};

		// Make separate methods for each Actor type (character, npc, etc.) to keep
		// things organized.
		this._prepareKingdomData(actorData);
		this._prepareCharacterData(actorData);
		this._prepareExtraData(actorData);
	}
	
	_prepareKingdomData(actorData) {
		if( actorData.type !== MYK.KINGDOM ) {
			return;
		}
		const data = actorData.data;
		// TODO
	}

	_prepareCharacterData(actorData) {
		if( actorData.type !== MYK.CHARACTER ) {
			return;
		}

		// Make modifications to data here. For example:
		const data = actorData.data;
		data.hp_max = data.warfare + data.quest + data.level + 5;
		data.capacity = data.wit + Math.ceil(data.charisma / 2);
		data.defense = data.quest + 7;
		data.followers_max = data.charisma * 5 + data.level;
	}

	_prepareExtraData(actorData) {
		if( actorData.type !== MYK.EXTRA ) {
			return;
		}
		const data = actorData.data;
		// TODO
	}

	/**
	 * This is for getting a data manipulated so that roll syntax is easier to write
	 */
	getRollData() {
		const data = super.getRollData();

		this._getKingdomRollData(data);
		this._getCharacterRollData(data);
		this._getExtraRollData(data);

		return data;
	}

	_getKingdomRollData(data) {
		if( data.type !== MYK.KINGDOM ) {
			return;
		}
		// TODO
	}

	_getCharacterRollData(data) {
		if( data.type !== MYK.CHARACTER ) {
			return;
		}
		// TODO
	}

	_getExtraRollData(data) {
		if( data.type !== MYK.EXTRA ) {
			return;
		}
		// TODO
	}
}