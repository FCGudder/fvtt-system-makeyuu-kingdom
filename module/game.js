import { Character } from "./character.js";
import { CharacterSheet } from "./character-sheet.js";
import { MYK } from "./constants.js";

Hooks.once("init", async function() {
	console.log(`Initializing Meikyuu Kingdom System`);

	game.myk = {
		Character
	};

	CONFIG.Actor.documentClass = Character;

	/* Register sheet application classes */
	Actors.unregisterSheet("core", ActorSheet);
	Actors.registerSheet(MYK.GAME, CharacterSheet, { makeDefault: true });

});
