import { MYK } from "./constants.js";

export class CharacterSheet extends ActorSheet {
	/** @inheritdoc */
	static get defaultOptions() {
		return foundry.utils.mergeObject(super.defaultOptions, {
			classes: [MYK.GAME, "sheet", "actor"],
			template: "systems/" + MYK.GAME + "/templates/actors/character-sheet.html",
			width: 600,
			height: 600,
			tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description"}]
		});
	}

	get template() {
		return "systems/" + MYK.GAME + "/templates/actors/" + this.actor.data.type + "-sheet.html";
	}
	
	getData() {
		const context = super.getData();
		const actorData = this.actor.data.toObject(false);
		context.data = actorData.data;
		context.flags = actorData.flags;
		context.rollData = context.actor.getRollData();
		/* additional stuff for display only goes here */
		return context;
	}

	activateListeners(html) {
		super.activateListeners(html);
		/* add interactive stuff here. jQuery FTW */
	}
}
